This module allows you to define a new node type called a 'Jeopardy Game', 
which lets your authorized users create Jeopardy games for other users to play later.

You choose a number of categories and a number of questions per category on one screen, then 
you are redirected to a form to fill in the category names, and the question (along with an
answer field) for each question.

If you choose to update the number of categories and/or questions, you can potentially lose
information stored in questions.  You will also have to save the node with the new numbers,
then re-edit the form, and additional category, question, and answer boxes may be available.

When you view  a Jeopardy game it requires JavaScript to function properly.  One click on a cell
in the table shows the question, another click on the word 'answer' shows the answer.

The idea is that you can use a projector or some similar means to show the Jeopardy game to
your class, then you will be in charge of when a question is displayed.  If you let your students
play the game independently, be forewarned that a simple 'view source' by your students will let
them cheat.

No more further development of this module is likely to happen (unless someone finds a huge bug).
