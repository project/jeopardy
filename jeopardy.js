
// Global JS Kill Switch
if (Drupal.jsEnabled) {
  $(document).ready(function() {
    $('td.jeopardy-table-question').click(showAnswer);
    $('td.jeopardy-table-question').mouseover(function () {
      $(this).addClass('jeopardy-over');
    });
    $('td.jeopardy-table-question').mouseout(function () {
      $(this).removeClass('jeopardy-over');
    });
  });
}

/**
 * Bind a click event to each td to show the matching question
 */
function showAnswer() {
  if ($(".answer-link").size() > 0) {
    return;
  }
  var td = this;
  $(td).unbind('click', showAnswer);
  var loc = this.id.replace('jeopardy-', '');
  $('#question-' + loc).each(function () {
    var el = this;
    $('#hidden-jeopardy').each(function () {
      this.innerHTML = '<div class="question-wrapper">' + el.innerHTML + '</div>';
    });
    $(td).addClass("question-visited");
    $(td).append('<div class="answer-wrapper"><a class="answer-link" id="show-answer-' + loc + '">Answer</a></div>');
    $('#show-answer-' + loc).click(function () {
      $('#answer-' + loc).each(function () {
        var el = this;
        $('#hidden-jeopardy').each(function () {
          this.innerHTML = '<div class="question-wrapper">' + el.innerHTML + '</div>';
        });
        $(td).removeClass("question-visited").addClass("answer-visited");
      });
      $(this).parent().remove();
    });
  });
}
